// Middlewares

function logRouteCall(req, res, next) {
    console.log('A route has been requested');
    next();
}

module.exports = {
    logRouteCall,
}